package br.com.bancomalula;

public class BancoTest {
	
	public static void main(String[] args) {
		
		
		Conta.saldoDoBanco = 2_000_000_000.00;
		
		Conta conta = new Conta(1, "Corrente", 2_750.99, "123", new Cliente("Jo�o", "52.899.977-1", "49095654850", "joao@senai.com", Sexo.MASCULINO));
		
		conta.exibeSaldo();
		conta.deposita(50);
		conta.exibeSaldo();
		System.out.println("Confre:" + Conta.saldoDoBanco);
		conta.saca(200);
		conta.exibeSaldo();
		System.out.println("Confre:" + Conta.saldoDoBanco);
		
		System.out.println("DADOS DA CONTA");
		System.out.println("N          " + conta.getNumero());
		System.out.println("Tipo:      " + conta.getTipo());
		System.out.println("Senha:     " + conta.getSenha());
		System.out.println("Saldo:     "+ conta.getSaldo());
		System.out.println("Titular:   " + conta.getCliente().getNome());
		System.out.println("CPF:       "+ conta.getCliente().getCpf());
		System.out.println("RG:        " + conta.getCliente().getRg());
		System.out.println("Email:     " + conta.getCliente().getEmail());
		System.out.println("Sexo:      " + conta.getCliente().getSexo().nome);
		
		
	}
}
